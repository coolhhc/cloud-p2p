{-# LANGUAGE DeriveDataTypeable #-}
module Control.Distributed.Backend.P2PHandler (
  Action(..),
  HandlerMsg(..)
  ) where

import Data.Typeable
import Data.Binary (Binary(put, get), putWord8, getWord8)
import Control.Applicative (Applicative, (<$>), (<*>))

import Control.Distributed.Process                as DP


data Action = GetPeers
            | SendMessage   { getMsg   :: String }
            | SearchArticle { getQuery :: String }
  deriving (Show, Eq, Ord)

data HandlerMsg = HandlerMsg {
    handlerPid    :: ProcessId,
    handlerAction :: Action
  } deriving (Show, Eq, Ord, Typeable)

instance Binary HandlerMsg where
  put hmsg = put (handlerPid hmsg) >> put (handlerAction hmsg)
  get      = HandlerMsg <$> get <*> get 

instance Binary Action where
  put GetPeers            = putWord8 0
  put (SendMessage msg)   = putWord8 1 >> put msg
  put (SearchArticle msg) = putWord8 2 >> put msg
  get = do
    header <- getWord8
    case header of
      0 -> return GetPeers
      1 -> SendMessage   <$> get
      2 -> SearchArticle <$> get
      _ -> fail "Identifier.get: invalid"
